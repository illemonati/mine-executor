package tech.tioft.mineExecutor

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender

class StartExecutable: CommandExecutor {


    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        ProcessBuilder(*args).start()
        Bukkit.getLogger().info(args.joinToString(" "))
        sender.sendMessage("started " + args.joinToString(" "))

        return true;
    }


}
