package tech.tioft.mineExecutor

import org.bukkit.plugin.java.JavaPlugin

class MineExecutor() : JavaPlugin() {
    override fun onEnable() {
        super.onEnable()
        getCommand("start-exe")?.setExecutor(StartExecutable())
        logger.info("tech.tioft.MineExecutor.MineExecutor enabled")
    }
}
